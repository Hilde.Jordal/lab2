package INF101.lab2;

import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	
	int maxCapacity = 20;
	
	ArrayList<FridgeItem> fridgeContent;
	
	// I think this is a constructor
	public Fridge() {
		fridgeContent = new ArrayList<>();
	}

	@Override
	public int nItemsInFridge() {
		return fridgeContent.size();
	}

	@Override
	public int totalSize() {
		return maxCapacity;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		if (nItemsInFridge() < maxCapacity) {
			fridgeContent.add(item);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void takeOut(FridgeItem item) {
		if (fridgeContent.isEmpty()) {
			throw new NoSuchElementException();
		} else {
			fridgeContent.remove(item);
		}
	}

	@Override
	public void emptyFridge() {
		fridgeContent.clear();
	}

	@Override
	public ArrayList<FridgeItem> removeExpiredFood() {
		ArrayList<FridgeItem> expiredItems = new ArrayList<>();
		for (FridgeItem item : fridgeContent) {
			if (item.hasExpired()) {
				expiredItems.add(item);
			}
		}
		fridgeContent.removeAll(expiredItems);
		return expiredItems;
	}
		
	
}
